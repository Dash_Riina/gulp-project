// $(document).ready(function(){
//     var lat = '';
//     var lon = '';
//     var city = '';
//     $.get("https://ipinfo.io/json",function(response){
//         lat = response.loc.split(',')[0];
//         lon = response.loc.split(',')[1];
//         city = response.city;
//         console.log(lat,lon);
//         // $.get("http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=bf60c7cca9ba7d27aa20f720b3d78bec",function(res){
//         //     console.log(res);
//         // });
//         $.get("http://api.openweathermap.org/data/2.5/weather?q="+city+"&appid=bf60c7cca9ba7d27aa20f720b3d78bec", function(res) {
//             console.log(res);
//             $(".city").html(city);
//             var temp = Math.round(res.main.temp) - 273;
//             $(".temp").html(temp+'&#8451;');
//             var name = res.weather[0]['main'];
//             $(".weather-name").html(name);
//             var icon = res.weather[0].icon;
//             $(".weather-img").attr('src',`http://openweathermap.org/img/w/${icon}.png`);
//         });
//     })
// })

// получаем погоду по заданым координатам

fetch('https://ipinfo.io/json')
    .then(function(response){
        return response.json();
    })
    .then(function(ipdata){
        // console.log(ipdata);
        let lat = ipdata.loc.split(',')[0];
        let lon = ipdata.loc.split(',')[1];
        return { lat, lon}
    })
    .then(function(coords){
        fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${coords.lat}&lon=${coords.lon}&appid=bf60c7cca9ba7d27aa20f720b3d78bec`)
            .then(function(weatherresp){
                return weatherresp.json();
            })
            .then(function(weatherdata){
                console.log(weatherdata);
            })
    })