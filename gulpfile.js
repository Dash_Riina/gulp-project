// храним все галп таски, которые будем использовать, первый пример для Хелоу

// let gulp = require('gulp');
//
// gulp.task('test', function(){
//     console.log('hello');
// })
//
// let gulp = require('gulp');
//     browserSync = require('browser-sync').create();
//
// gulp.task('test', function(){
//     console.log('hello');
// })
//
// gulp.task('copy-html', function (){
//     gulp.src('./src/**/*.html').pipe(gulp.dest('./dist'));
// })
//
//
// gulp.task('serve', function () {
//     browserSync.init({
//         server: {
//             baseDir: "./dist",
//             port: 3010
//         }
//     });
//
//     gulp.watch("./src/*.html",['copy-html']).on('change', browserSync.reload);
// })

'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    pump = require('pump'),
    minifyJS = require('gulp-js-minify'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin')

    gulp.task('sass', function() {
    return gulp.src("./src/scss/**/*.scss")
        .pipe(sass())
        .pipe(gulp.dest("./dist/css"));
});
gulp.task('copy-html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist'));
});
gulp.task('copy-js', function () {
    gulp.src('./src/js/**/*.js')
        .pipe(gulp.dest('./dist/js'));
});
gulp.task('copy-img', function () {
    gulp.src('./src/img/**/*.*')
        .pipe(gulp.dest('./dist/img'));
});
gulp.task('serve', ['sass','copy-html','copy-js','copy-img'], function() {

    browserSync.init({
        server: "./dist"
    });
    gulp.watch('./src/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
    gulp.watch("./src/*.html", ['copy-html']).on('change', browserSync.reload);
    gulp.watch("./src/js/*.js", ['copy-js']).on('change', browserSync.reload);
});




