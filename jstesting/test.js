describe('pow', function(){
    it("2^3=8", function() {
        assert.equal(pow(2, 3), 8);
    });
    it("2^5=32", function() {
        assert.equal(pow(2, 5), 32);
    });'
    it('test1',function(){
        assert.equal(pow(2,1),2)
    });
    it('test2',function(){
        assert.equal(pow(2,2),4)
    });
    describe('Тестирование натуральных степеней',function(){
        var res = 2;
        for (var i=1;i<10;i++) {
            it("test"+i,function(){
                assert.equal(pow(2,i), res);
            })
            res*=2;
        }
    });
    describe('Тестируем 0',function(){
        for (var i=1;i<9;i++) {
            it('тесты для нуля #'+i,function(){
                assert((pow(i,0)==1),'не 1');
            })
        }
    })

    it('test frac',function(){
        assert(isNaN(pow(3,2.3)), 'Not NaN');
    });
    it('test negative', function(){
        assert(isNaN(pow(5,-3)), 'Not NaN too');
    })
});