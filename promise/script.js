// использование промисов для отслеживания ошибки
let promise = new Promise(function(resolve, reject) {
    // setTimeout(()=>resolve('done'), 1000);
    setTimeout(()=>reject(new Error()), 1000);

});

promise.then(
    result=>console.log(result),
    error=>console.log(error)
);




// chain for promises

let promise = new Promise(function (resolve, reject) {
    resolve(1);
})

promise
    .then(function (result) {console.log(result);return result*2})
    .then(function (result) {console.log(result);return result*2})
    .then(function (result) {console.log(result);return result*2})
    .then(function (result) {console.log(result);return result*2})


делаем delay с выводом 3 сек
delay(ms) {}
delay(3000).then(()=> alert('3'));


function delay(ms) {
    return new Promise(function (resolve, reject) {
        setTimeout(()=>alert('${Math.round(ms/1000)}s'),
            ms);
    })
}

delay(1000).then();

function loadScript(url) {
    return new Promise(function (resolve, reject){
        let script = document.createElement('script');
        script.setAttribute('src', url);
        document.body.appendChild(script);
        resolve(1);
    });
}

let jqueryScript = loadScript('https://code.jquery.com/jquery-3.3.1.min.js');

jqueryScript.then(
    result => console.log('script is loaded'),
    error => console.log('error loading script')
)


